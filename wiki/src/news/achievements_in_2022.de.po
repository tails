# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-08 01:30+0000\n"
"PO-Revision-Date: 2024-11-13 13:37+0000\n"
"Last-Translator: Benjamin Held <Benjamin.Held@protonmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"What we achieved together in 2022\"]]\n"
msgstr "[[!meta title=\"Was wir gemeinsam im Jahr 2022 erreicht haben\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Thu, 23 Nov 2022 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Thu, 23 Nov 2022 12:34:56 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
#, no-wrap
msgid "**If a tool is hard to use, it is less secure.**\n"
msgstr "**Wenn ein Werkzeug schwer zu bedienen ist, ist es weniger sicher.**\n"

#. type: Plain text
msgid ""
"When digital security tools are too complicated to use, people make mistakes "
"that compromise their security or simply give up and fall back on more "
"dangerous practices."
msgstr ""
"Wenn digitale Sicherheitstools zu kompliziert zu bedienen sind, machen "
"Menschen Fehler, die ihre Sicherheit gefährden, oder sie geben einfach auf "
"und greifen auf riskantere Praktiken zurück."

#. type: Plain text
msgid ""
"Because journalists and whistleblowers use Tails to expose abuse of power "
"and activists use Tails to defend human rights and our planet, the more "
"people use Tails, the better we are all protected."
msgstr ""
"Da Journalist:innen und Whistleblower Tails nutzen, um Machtmissbrauch "
"aufzudecken, und Aktivist:innen Tails verwenden, um Menschenrechte und "
"unseren Planeten zu verteidigen, gilt: Je mehr Menschen Tails verwenden, "
"desto besser sind wir alle geschützt."

#. type: Plain text
msgid ""
"In 2022, we focused on making it easier to install Tails and improving many "
"existing features, instead of adding new features. Here are some highlights."
msgstr ""
"Im Jahr 2022 konzentrierten wir uns darauf, die Installation von Tails zu "
"erleichtern und viele bestehende Funktionen zu verbessern, anstatt neue "
"Funktionen hinzuzufügen. Hier sind einige Höhepunkte."

#. type: Plain text
msgid ""
"Our work is only possible thanks to the donations of our many supporters. In "
"2020&ndash;2022, [[donations from passionate people like you|donate]] "
"represented 76% of our income. This is the best proof of the value of our "
"work and our biggest motivation."
msgstr ""
"Unsere Arbeit ist nur dank der Spenden unserer vielen Unterstützer möglich. "
"In den Jahren 2020&ndash;2022 machten [[Spenden von leidenschaftlichen "
"Menschen wie Ihnen|donate]] 76 % unseres Einkommens aus. Dies ist der beste "
"Beweis für den Wert unserer Arbeit und unsere größte Motivation."

#. type: Plain text
msgid "Thanks!"
msgstr "Danke!"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Title #
#, no-wrap
msgid "Better tools"
msgstr "Bessere Werkzeuge"

#. type: Title ##
#, no-wrap
msgid "*Persistent Storage*"
msgstr "*Beständiger Datenspeicher*"

#. type: Plain text
msgid ""
"The biggest change is still to come as we will release a complete redesign "
"of the Persistent Storage in Tails 5.8 (December 20). You can already test "
"the new Persistent Storage in [[Tails 5.8~beta1|test_5.8-beta1]]."
msgstr ""
"Die größte Veränderung steht noch bevor, da wir am 20. Dezember Tails 5.8 "
"mit einem kompletten Redesign des beständigen Datenspeichers veröffentlichen "
"werden. Sie können den neuen beständigen Datenspeicher bereits in [[Tails 5."
"8~beta1|test_5.8-beta1]] testen."

#. type: Plain text
msgid ""
"The Persistent Storage hadn't changed much since its first release in 2012 "
"because the code was hard to modify and improve. But, we learned from users "
"that the Persistent Storage could do a lot more for you if it had more "
"features and was easier to use."
msgstr ""
"Der beständige Datenspeicher hatte sich seit seiner ersten Veröffentlichung "
"im Jahr 2012 nicht viel verändert, da der Code schwer zu modifizieren und zu "
"verbessern war. Doch wir haben von den Benutzern gelernt, dass der "
"beständige Datenspeicher viel mehr für Sie tun könnte, wenn er mehr "
"Funktionen hätte und benutzerfreundlicher wäre."

#. type: Plain text
msgid ""
"The new Persistent Storage won't require restarting after creating it or "
"each time you activate a new feature."
msgstr ""
"Der neue beständige Datenspeicher erfordert keinen Neustart mehr, nachdem "
"Sie ihn erstellt haben oder jedes Mal, wenn Sie eine neue Funktion "
"aktivieren."

#. type: Plain text
msgid ""
"It will also make it easier for us to persist more settings in the future."
msgstr ""
"Außerdem wird es für uns einfacher sein, in Zukunft weitere Einstellungen "
"vorzunehmen."

#. type: Plain text
msgid "And finally, it looks so much better!"
msgstr "Und schließlich sieht es so viel besser aus!"

#. type: Plain text
#, no-wrap
msgid "[[!img test_5.8-beta1/create.png link=\"no\" alt=\"\"]]\n"
msgstr "[[!img test_5.8-beta1/create.png link=\"no\" alt=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!img test_5.8-beta1/features.png link=\"no\" alt=\"\"]]\n"
msgstr "[[!img test_5.8-beta1/features.png link=\"no\" alt=\"\"]]\n"

#. type: Title ##
#, no-wrap
msgid "PGP tools"
msgstr "PGP-Tools"

#. type: Plain text
msgid ""
"We know that lots of our users depend on PGP tools to protect their "
"communications. But, we also know that PGP tools are notoriously hard to use."
msgstr ""
"Wir wissen, dass viele unserer Benutzer auf PGP-Tools angewiesen sind, um "
"ihre Kommunikation zu schützen. Aber wir wissen auch, dass PGP-Tools "
"bekanntermaßen schwer zu bedienen sind."

#. type: Plain text
msgid "We made 2 big changes to PGP tools in Tails:"
msgstr "Wir haben 2 große Änderungen an den PGP-Tools in Tails vorgenommen:"

#. type: Bullet: '- '
msgid ""
"We added [[*Kleopatra*|doc/encryption_and_privacy/kleopatra]], a swiss army "
"knife for all things PGP."
msgstr ""
"Wir haben [[*Kleopatra*|doc/encryption_and_privacy/kleopatra]] hinzugefügt, "
"ein Schweizer Taschenmesser für alles rund um PGP."

#. type: Plain text
#, no-wrap
msgid ""
"  *Kleopatra* replaced the *OpenPGP Applet* and the *Password and Keys*\n"
"  utility, also known as *Seahorse*, which were not actively developed anymore\n"
"  and had many issues.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img version_5.0/kleopatra.png alt=\"\" link=\"no\"]]\n"
msgstr ""

#. type: Bullet: '- '
msgid ""
"We updated *Thunderbird* to 102, which includes major usability improvements "
"to the OpenPGP feature."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  When composing an email, you can now see whether it will be encrypted or not.\n"
"  If encryption is impossible, a key assistant helps you solve key issues.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  [[!img version_5.5/thunderbird_102.png link=\"no\" alt=\"Thunderbird composer\n"
"  window with encryption turned on and notification about a missing key and how\n"
"  to resolve the issue.\"]]\n"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "*Tor Connection* assistant"
msgstr ""

#. type: Plain text
msgid ""
"We conducted usability tests of the *Tor Connection* assistant in France and "
"Brazil with people who were new to Tails to identify issues in complicated "
"cases: when connecting to Tor is blocked by censorship or a captive portal."
msgstr ""

#. type: Plain text
msgid ""
"We fixed [28 usability issues](https://gitlab.tails.boum.org/tails/tails/-/"
"issues/?"
"sort=closed_desc&state=closed&label_name%5B%5D=C%3ATor%20Connection&first_page_size=20)  "
"identified during these tests. For example:"
msgstr ""

#. type: Bullet: '- '
msgid ""
"The *Tor Connection* assistant now automatically fixes the computer clock if "
"you choose to connect to Tor automatically. This makes it much easier for "
"people in Asia to circumvent censorship."
msgstr ""

#. type: Bullet: '- '
msgid ""
"We made it easier to open the *Unsafe Browser* to sign in to a network. This "
"makes it much easier to use Tails on public networks in airports, libraries, "
"and so on."
msgstr ""

#. type: Bullet: '- '
msgid ""
"In Tails 5.8 (December 20), you will be able to scan a QR code from your "
"phone to enter a Tor bridge:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  [[!img test_5.8-beta1/qr_code.png link=\"no\" alt=\"\"]]\n"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "*Unsafe Browser* and Wayland"
msgstr "*Unsicherer Browser* und Wayland"

#. type: Plain text
msgid ""
"Tails 5.8 will also make the *Unsafe Browser* both safer and easier to use "
"by migrating it to the Wayland display technology."
msgstr ""
"Tails 5.8 wird den *Unsicheren Browser* auch sicherer und "
"benutzerfreundlicher machen, indem er auf die Wayland-Anzeigetechnologie "
"migriert wird."

#. type: Plain text
msgid ""
"Wayland brings more security in-depth to Tails by making it harder for a "
"compromised application in Tails to compromise or misuse another application."
msgstr ""
"Wayland bietet Tails eine tiefere Sicherheit, indem es es einer "
"kompromittierten Anwendung in Tails erschwert, eine andere Anwendung zu "
"kompromittieren oder zu missbrauchen."

#. type: Plain text
msgid ""
"Wayland also fixes other features that were not working yet in the *Unsafe "
"Browser*:"
msgstr ""
"Wayland behebt auch andere Funktionen, die im *Unsicheren Browser* noch "
"nicht funktionierten:"

#. type: Plain text
msgid ""
"- Sound - Uploads and downloads - Alternative input methods for Chinese and "
"other non-Latin languages - Accessibility features like the screen reader "
"and virtual keyboard"
msgstr ""
"- Sound - Uploads und Downloads - Alternative Eingabemethoden für Chinesisch "
"und andere nicht-lateinische Sprachen - Barrierefreiheitsfunktionen wie der "
"Bildschirmleser und die virtuelle Tastatur"

#. type: Title ##
#, no-wrap
msgid "Metadata cleaning"
msgstr "Metadatenreinigung"

#. type: Plain text
msgid ""
"We added *Metadata Cleaner*, a new tool to clean metadata from your files."
msgstr ""
"Wir haben *Metadata Cleaner* hinzugefügt, ein neues Werkzeug, um Metadaten "
"aus Ihren Dateien zu entfernen."

#. type: Plain text
#, no-wrap
msgid "[[!img version_5.7/metadata_cleaner.png link=\"no\" alt=\"\"]]\n"
msgstr "[[!img version_5.7/metadata_cleaner.png link=\"no\" alt=\"\"]]\n"

#. type: Plain text
msgid ""
"When we switched to *MAT* 0.8.0 in Tails 4.0, *MAT* lost its graphical "
"interface and was only accessible from the contextual menu of the *Files* "
"browser. It became especially hard for new users to learn how to clean their "
"files. *Metadata Cleaner* fixes this by providing a simple and easily "
"discoverable graphic interface to remove metadata."
msgstr ""
"Als wir in Tails 4.0 auf *MAT* 0.8.0 umschalteten, verlor *MAT* seine "
"grafische Benutzeroberfläche und war nur noch über das Kontextmenü des "
"*Dateien*-Browsers zugänglich. Es wurde besonders schwierig für neue Nutzer, "
"zu lernen, wie sie ihre Dateien bereinigen können. *Metadata Cleaner* behebt "
"dies, indem es eine einfache und leicht auffindbare grafische Oberfläche zum "
"Entfernen von Metadaten bereitstellt."

#. type: Plain text
#, no-wrap
msgid ""
"*Metadata Cleaner* works on the same file formats and is as secure as *MAT*\n"
"because *Metadata Cleaner* also uses *MAT* in the background to do the actual\n"
"cleaning.\n"
msgstr ""
"*Metadata Cleaner* funktioniert mit denselben Dateiformaten und ist ebenso "
"sicher wie *MAT*, \n"
"da *Metadata Cleaner* ebenfalls *MAT* im Hintergrund verwendet, um die "
"tatsächliche Bereinigung durchzuführen.\n"

#. type: Title ##
#, no-wrap
msgid "Backups"
msgstr ""

#. type: Plain text
msgid ""
"We added a utility to make a backup of the Persistent Storage to another "
"Tails USB stick."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img version_4.25/backup.png link=\"no\" alt=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"It's pretty basic. We still want to do something better in [[!tails_ticket\n"
"7049]], but we didn't want to wait more because we know that backups are a big\n"
"issue for our users.\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Easier installation"
msgstr "Einfachere Installation"

#. type: Title ##
#, no-wrap
msgid "Installation instructions"
msgstr "Installationsanweisungen"

#. type: Plain text
msgid ""
"During usability tests in Mexico and Brazil, we realized that our "
"installation process was already pretty easy to follow, but that some people "
"were getting lost while navigating between the different pages."
msgstr ""
"Während der Usability-Tests in Mexiko und Brasilien stellten wir fest, dass "
"unser Installationsprozess bereits recht einfach zu befolgen war, aber "
"einige Personen beim Navigieren zwischen den verschiedenen Seiten verloren "
"gingen."

#. type: Plain text
msgid ""
"We simplified our instructions by making them into a [[single page|install/"
"windows]]."
msgstr ""
"Wir haben unsere Anweisungen vereinfacht, indem wir sie auf eine [[einzige "
"Seite|install/windows]] zusammengefasst haben."

#. type: Plain text
msgid "We added animations and more exciting visuals."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<video autoplay loop muted width=\"200\" height=\"200\" poster=\"https://tails.net/install/inc/success/install.png\">\n"
"  <source src=\"https://tails.net/install/inc/success/install.mp4\" type=\"video/mp4\" />\n"
"</video>\n"
msgstr ""
"<video autoplay loop muted width=\"200\" height=\"200\" poster=\"https://tails.net/install/inc/success/install.png\">\n"
"  <source src=\"https://tails.net/install/inc/success/install.mp4\" type=\"video/mp4\" />\n"
"</video>\n"

#. type: Plain text
msgid ""
"Finally, we fixed no less than [30 other smaller usability issues](https://"
"gitlab.tails.boum.org/tails/tails/-/issues/?"
"sort=closed_desc&state=closed&label_name%5B%5D=C%3AInstallation&first_page_size=20) "
"in the instructions!"
msgstr ""
"Schließlich haben wir nicht weniger als [30 weitere kleinere "
"Benutzerfreundlichkeitsprobleme](https://gitlab.tails.boum.org/tails/tails/-/"
"issues/?sort=closed_desc&state=closed&label_name%5B%5D=C%3AInstallation&first"
"_page_size=20) in den Anweisungen behoben!"

#. type: Title ##
#, no-wrap
msgid "Tails.net and more reliable downloads"
msgstr "Tails.net und zuverlässigere Downloads"

#. type: Plain text
msgid ""
"After 13 years of relying on the domain name of our historical hosting "
"provider, *boum.org*, we finally acquired a simple, beautiful, and "
"trustworthy domain name of our own:"
msgstr ""
"Nach 13 Jahren, in denen wir uns auf den Domainnamen unseres historischen "
"Hosting-Anbieters *boum.org* verlassen haben, haben wir endlich einen "
"einfachen, schönen und vertrauenswürdigen Domainnamen für uns selbst "
"erworben:"

#. type: Plain text
msgid "🎉 ✨ ***tails.net*** ✨ 🎉"
msgstr "🎉 ✨ ***tails.net*** ✨ 🎉"

#. type: Plain text
msgid "It will take us some time to migrate all our services to this new name."
msgstr ""
"Es wird einige Zeit in Anspruch nehmen, all unsere Dienste auf diesen neuen "
"Namen umzustellen."

#. type: Plain text
msgid ""
"As a start, we deployed <https://download.tails.net/>. This new service uses "
"[Mirrorbits](https://github.com/etix/mirrorbits), a more reliable way of "
"redirecting downloads to one of our mirrors. It prevents many broken "
"downloads and automates some tedious work."
msgstr ""
"Zu Beginn haben wir <https://download.tails.net/> bereitgestellt. Dieser "
"neue Dienst verwendet [Mirrorbits](https://github.com/etix/mirrorbits), eine "
"zuverlässigere Methode, um Downloads an einen unserer Spiegelserver "
"weiterzuleiten. Dadurch werden viele fehlerhafte Downloads verhindert und "
"einige mühsame Aufgaben automatisiert."

#. type: Title #
#, no-wrap
msgid "Team life"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "New team members"
msgstr "Neue Teammitglieder"

#. type: Plain text
msgid "Our team grew and welcomed 2 new full-time workers:"
msgstr ""
"Unser Team ist gewachsen und hat 2 neue Vollzeitmitarbeiter willkommen "
"geheißen:"

#. type: Bullet: '- '
msgid ""
"[@disoj](https://gitlab.tails.boum.org/disoj) joined us as our Project "
"Manager in April."
msgstr ""
"[@disoj](https://gitlab.tails.boum.org/disoj) trat im April als unser "
"Projektmanager zu uns."

#. type: Plain text
#, no-wrap
msgid ""
"  She will help our team grow, mature, and ultimately serve our users better.\n"
"  It's the first time that we hired for a managerial position, so it's a very\n"
"  big and positive change.\n"
msgstr ""
"  Sie wird unserem Team helfen, zu wachsen, sich weiterzuentwickeln und "
"letztendlich unseren Nutzern besser zu dienen.  \n"
"  Es ist das erste Mal, dass wir eine Führungsposition besetzen, daher ist "
"es eine sehr große und positive Veränderung.\n"

#. type: Bullet: '- '
msgid ""
"[@groente](https://gitlab.tails.boum.org/groente), a long-term Sysadmin "
"consultant, joined us full-time in July."
msgstr ""
"[@groente](https://gitlab.tails.boum.org/groente), ein langfristiger "
"Sysadmin-Berater, trat im Juli in Vollzeit zu uns."

#. type: Plain text
#, no-wrap
msgid ""
"  He will help us deploy better infrastructure to make our work more efficient\n"
"  while keeping all our machines (and thus your Tails) secure.\n"
msgstr ""
"  Er wird uns helfen, eine bessere Infrastruktur bereitzustellen, um unsere "
"Arbeit effizienter zu gestalten und gleichzeitig alle unsere Maschinen (und "
"damit Ihr Tails) sicher zu halten.\n"

#. type: Title ##
#, no-wrap
msgid "Organizational redesign"
msgstr "Organisatorische Umgestaltung"

#. type: Plain text
msgid ""
"Behind the curtains, Tails operates as a small team that values "
"transparency, autonomy, and horizontal decision-making. We have always "
"worked fully remotely and now have team members in 10 different countries."
msgstr ""
"Hinter den Kulissen arbeitet Tails als kleines Team, das Transparenz, "
"Autonomie und horizontale Entscheidungsfindung schätzt. Wir haben immer "
"vollständig remote gearbeitet und haben jetzt Teammitglieder in 10 "
"verschiedenen Ländern."

#. type: Plain text
msgid "All this can be challenging sometimes :)"
msgstr "Das kann manchmal herausfordernd sein :)"

#. type: Plain text
msgid ""
"This year, we redesigned, clarified, and documented our internal organizing "
"a lot. In particular, it is now clearer who is responsible, and legitimate, "
"to make which decisions. This will make Tails a more pleasant and efficient "
"working place, so we can focus more of our energy on improving our tools."
msgstr ""
"In diesem Jahr haben wir unsere interne Organisation umfassend überarbeitet, "
"klargestellt und dokumentiert. Insbesondere ist jetzt deutlicher, wer für "
"welche Entscheidungen verantwortlich und legitim ist. Dies wird Tails zu "
"einem angenehmere und effizienteren Arbeitsplatz machen, sodass wir mehr "
"Energie darauf verwenden können, unsere Werkzeuge zu verbessern."

#. type: Title #
#, no-wrap
msgid "We never let you down!"
msgstr ""

#. type: Plain text
msgid ""
"To always keep you safe, a significant part of our work consists in "
"releasing Tails every 4 weeks, updating to new version of *Tor Browser* and "
"the *Tor* client, migrating to new technologies in GNOME or Debian, etc."
msgstr ""

#. type: Plain text
msgid "We released:"
msgstr ""

#. type: Plain text
msgid "- 14 releases, never skipping a single update of *Tor Browser*."
msgstr ""

#. type: Plain text
msgid ""
"- Tails 5.0 based on Debian Bullseye, with important technical migrations."
msgstr ""

#. type: Bullet: '- '
msgid ""
"2 emergency releases to fix critical vulnerabilities in the *Tor* client and "
"the *Linux* kernel."
msgstr ""

#. type: Plain text
msgid ""
"All this work has been made possible by donations from users like you. If "
"you like these changes and want more, donate now to fund our work in 2023."
msgstr ""

#. type: Plain text
msgid "[[Donate]]"
msgstr "[[Spenden|donate]]"

#, no-wrap
#~ msgid "</div>\n"
#~ msgstr "</div>\n"
