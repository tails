# German translation of basewiki/sandbox page for ikiwiki.
# Copyright © 2010 Sebastian Kuhnert <mail@sebastian-kuhnert.de>
# Redistribution and use in source and compiled forms, with or without
# modification, are permitted under any circumstances. No warranty.
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"POT-Creation-Date: 2024-06-28 14:46+0000\n"
"PO-Revision-Date: 2024-09-28 16:09+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: None\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
msgid "This is the SandBox."
msgstr "Aquest és l'Entorn de Proves."

#. type: Plain text
#, no-wrap
msgid "*emphasised* text.\n"
msgstr "text *ressaltat*.\n"

#. type: Title #
#, no-wrap
msgid "GitLab syntax"
msgstr "Sintaxi de GitLab"

#. type: Plain text
msgid "- MR: !123"
msgstr "- MR: !123"

#. type: Plain text
msgid "- issue: #18079"
msgstr "- issue: #18079"

#. type: Title #
#, no-wrap
msgid "Header"
msgstr "Capçalera"

#. type: Plain text
#, no-wrap
msgid ""
"> This is a blockquote.\n"
">\n"
"> This is the first level of quoting.\n"
">\n"
"> > This is nested blockquote.\n"
">\n"
"> Back to the first level.\n"
msgstr ""
"> Això és un bloc de cita.\n"
">\n"
"> Aquest és el primer nivell de citació.\n"
">\n"
"> > Això és un bloc de cita imbricat.\n"
">\n"
"> De tornada al primer nivell.\n"

#. type: Plain text
msgid "Numbered list:"
msgstr "Llista numerada:"

#. type: Bullet: '1. '
msgid "First item."
msgstr "Primer element."

#. type: Bullet: '1. '
msgid "Another."
msgstr "Un altre."

#. type: Bullet: '  - '
msgid "nesting them"
msgstr "imbricant-los"

#. type: Bullet: '  - '
msgid "more nests"
msgstr "més imbricacions"

#. type: Bullet: '1. '
msgid "And another.."
msgstr "I un altre…"

#. type: Plain text
msgid "Bulleted list:"
msgstr "Llista de punts:"

#. type: Bullet: '* '
msgid "*item*"
msgstr "*element*"

#. type: Bullet: '* '
msgid "item"
msgstr "element"

#. type: Bullet: '  - '
msgid "1st item in nested list"
msgstr "1r element de la llista imbricada"

#. type: Bullet: '  - '
msgid "2nd item in nested list"
msgstr "2n element de la llista imbricada"

#. type: Bullet: '  - '
msgid "3rd item in nested list"
msgstr "3r element de la llista imbricada"

#. type: Title ##
#, no-wrap
msgid "Heading level 2"
msgstr "Encapçalament de nivell 2"

#. type: Plain text
msgid "Another test!!!"
msgstr "Una altra prova!"

#~ msgid "Internal:"
#~ msgstr "Intern:"

#~ msgid "<http://tails.net/bla>"
#~ msgstr "<http://tails.net/bla>"

#~ msgid "<https://tails.net/bla>"
#~ msgstr "<https://tails.net/bla>"

#~ msgid "<http://dl.amnesia.boum.org/bla>"
#~ msgstr "<http://dl.amnesia.boum.org/bla>"

#~ msgid "<https://dl.amnesia.boum.org/bla>"
#~ msgstr "<https://dl.amnesia.boum.org/bla>"

#~ msgid "<a href=\"http://example.com/bla\">bla</a>"
#~ msgstr "<a href=\"http://example.com/bla\">bla</a>"

#~ msgid "External:"
#~ msgstr "Extern:"

#~ msgid "<a href=\"http://example.com/ble\">ble</a>"
#~ msgstr "<a href=\"http://example.com/ble\">ble</a>"

#~ msgid ""
#~ "This is the SandBox, a page anyone can edit to learn how to use the wiki."
#~ msgstr ""
#~ "[[!meta title=\"Sandkasten\"]]\n"
#~ "Dies ist der Sandkasten: Eine Seite die jeder bearbeiten kann, um zu "
#~ "lernen wie man das Wiki benutzt."

#~ msgid "Here's a paragraph."
#~ msgstr "Dies ist ein Absatz."

#~ msgid "Here's another one with *emphasised* text."
#~ msgstr "Dies ist noch ein Absatz mit *hervorgehobenem* Text."

#~ msgid "Subheader"
#~ msgstr "Unterüberschrift"

#~ msgid "[[ikiwiki/WikiLink]]"
#~ msgstr "[[ikiwiki/WikiLink]]"
